#ifndef PATHS_HPP
#define PATHS_HPP

#include <string>

extern std::string prefixConfig;
extern std::string prefixCustom;
extern std::string prefixDefault;

void initPrefixes();

void makePaths();

std::string resolvePath(const char* path);

#endif
