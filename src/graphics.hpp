#ifndef GRAPHICS_H
#define GRAPHICS_H

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <glm/vec2.hpp>

#include <cv.hpp>
#include <model.hpp>
#include <modelpart.hpp>

extern GLuint transUniform;
extern float windowAspectRatio;

void initGraphics ();

void graphicsFrame ();

void initBuffers (GLuint* vaoNum);

void initTexture (GLuint* texNum, unsigned char* buffer, size_t bufferLength);

void initShader();

void printShaderCompileLog(GLuint shader);

void updateModel(struct FaceData faceData);

void showModelInfo();

#endif
