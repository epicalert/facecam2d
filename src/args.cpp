#include <args.hpp>
#include <config.hpp>
#include <iostream>

const char* argp_program_version =
	PROJECT_NAME " " VERSION_CODE "\n\n"
	"License: GPLv3 <https://www.gnu.org/licenses/gpl-3.0.html>\n"
	"If you did not receive a copy of the source code, it is available\n"
	"at <https://git.epicalert.xyz/Epicalert/facecam2d.git> or the\n"
	"GitLab mirror at <https://gitlab.com/epicalert/facecam2d.git>.";

#ifndef _WIN32
const struct argp_option options[] = {
	//name, key, arg, flags, doc, group
	{"haar-cascade",	0x00,	0,		0,	"Use Haar Cascades for faster (but less accurate) face detection.",	0},
	{"show-camera",		0x01,	0,		0,	"Show the camera feed in another window.",				0},
	{"no-eyes",		0x02,	0,		0,	"Disable eye tracking for better performance.",				0},
	{"model",		'm',	"model",	0,	"Name of the model file to use. (not including '.fma')",		0},
	// this option actually selects the minimum camera id to use,
	// i.e. instead of trying video0, video1, ... it will try
	// starting from 'c'.
	// e.g. -c6 is passed, so the program will try video6, video7, etc.
	{"camera",		'c',	"id",		0,	"ID number of camera to use. (e.g. /dev/videoXX where XX is the ID)",	0},
	{0}
};

struct argp argp = {
	options,
	parseOptions,
	0,
	0
};
#endif

struct optData optData = {
	false,
	false,
	false,
	"default",
	0,
};

#ifndef _WIN32
error_t parseOptions(int key, char* arg, struct argp_state* state) {
	switch (key) {
		case 0x00:	//--haar-cascade
			optData.useHaar = true;
			break;

		case 0x01:	//--show-camera
			optData.showCamera = true;
			break;

		case 0x02:	//--no-eyes
			optData.noEyes = true;
			break;

		case 'm':
			optData.model = std::string(arg);
			break;

		case 'c':
			optData.minCameraID = std::stoi(arg);
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}
#endif
