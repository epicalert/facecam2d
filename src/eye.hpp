#ifndef EYE_HPP
#define EYE_HPP

#include <opencv2/opencv.hpp>

#include<glm/vec2.hpp>

glm::vec2 eyeDirection(cv::Mat roi);

#endif
