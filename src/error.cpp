#include <iostream>
#include <stdlib.h>

#ifndef NO_GRAPHICAL_DIALOG
#include <boxer/boxer.h>
#endif

void showError(std::string message, std::string titleMessage, bool fatal = true) {
#ifndef NO_GRAPHICAL_DIALOG
	boxer::show(message.c_str(), titleMessage.c_str(), boxer::Style::Error);
#endif

	std::cerr << "ERROR";
	if (fatal) std::cerr << "(fatal)";
	std::cerr << ": " << message << std::endl;

	if (fatal) {
		exit(EXIT_FAILURE);
	}
}

void showWarning(std::string message, std::string titleMessage) {
#ifndef NO_GRAPHICAL_DIALOG
	boxer::show(message.c_str(), titleMessage.c_str(), boxer::Style::Warning);
#endif

	std::cerr << "WARN: " << message << std::endl;
}
