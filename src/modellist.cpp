#include <iostream>
#include <filesystem>

#include <paths.hpp>

#include <modellist.hpp>

// get models from a given directory and add them to a given vector
void getModelsFromDir(std::string path, std::vector<std::string>* vector) {
	// return if directory doesnt exist
	if (!std::filesystem::exists(path)) return;

	// iterate through all items in this directory
	for (auto& p: std::filesystem::directory_iterator(path)) {
		if (p.path().extension() == ".fma") {
			vector->push_back(p.path().stem().string());
		}
	}
}

std::vector<std::string> listModels() {
	std::vector<std::string> modelList;

	getModelsFromDir(prefixCustom + "models", &modelList);
	getModelsFromDir(prefixDefault + "models", &modelList);
	getModelsFromDir("models", &modelList);

	/*
	for (int i = 0; i < modelList.size(); i++) {
		std::cout << "Detected model: " << modelList.at(i) << std::endl;
	}
	*/

	return modelList;
}
