#ifndef CV_HPP
#define CV_HPP

#include <map>
#include <glm/vec2.hpp>

struct FaceData {
	std::map<int, glm::vec2> positions;
	std::map<int, bool> triggers;

	float headRotation;
	float scale;
};

void initCV();

void cvFrame();

void cvShowFrame();

#endif
