#ifndef MODELPART_HPP
#define MODELPART_HPP

#include <string>

#include <glm/ext/matrix_transform.hpp>
#include <graphics.hpp>
#include <cv.hpp>

#define BIND_NULL 0x00
#define BIND_HEAD 0x01
#define BIND_FACE 0x02

#define OFFSET_EYES 0x10


#define TRIGGER_NULL		0x00
#define TRIGGER_MOUTH_OPEN	0x01

// number of frames to average for smoothing
#define SMOOTHING_FRAMES	4

class ModelPart {
	GLuint vao;

	GLuint tex[16];	//support 16 textures to switch between
	int texTriggers[16];
	size_t texSelection = 0;
	size_t texCount = 0;

	glm::mat4 transMatrix = glm::mat4(1.0f);

	bool empty = true;

	glm::vec2 histPositions[SMOOTHING_FRAMES];
	float histRotations[SMOOTHING_FRAMES];
	float histScales[SMOOTHING_FRAMES];
	size_t histI = 0;
	
	public:
		int bind = BIND_NULL;
		int follow = BIND_NULL;
		float factor = 0.0f;	//default factor of 0 so part will not follow a null by default
		int offsetBind = BIND_NULL;
		float offsetFactor = 0.0f;

		float rotFactor = 1.0f;
		float scaleFactor = 1.0f;

		glm::vec2 posOffset = glm::vec2(0,0);
		glm::vec2 scaleOffset = glm::vec2(0,0);

		glm::vec2 origin = glm::vec2(0,0);


		ModelPart();

		void bindAndDraw();

		void setBind(std::string bindName);
		void setFollowTarget(std::string followTarget);
		void setOffsetBind(std::string bindName);

		void smoothTransform(glm::vec2 position, float rotation, float scale);
		void setTransform(glm::vec2 position, float rotation, float scale);

		void processFaceData(struct FaceData faceData);

		void addTexture(unsigned char* texBuffer, size_t texLength, size_t slot, std::string triggerName);
		void selectTexture(size_t slot);
};

#endif
