#include <input.hpp>
#include <graphics.hpp>

void keyInput(unsigned char key, int x, int y) {
	switch(key) {
		case 'i':	// model info
			showModelInfo();
			break;
		default:
			break;
	}
}
