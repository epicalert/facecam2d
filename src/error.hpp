#ifndef ERROR_HPP
#define ERROR_HPP

void showError(std::string message, std::string titleMessage, bool fatal = true);
void showWarning(std::string message, std::string titleMessage);

#endif
