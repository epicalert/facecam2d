#include <vector>
#include <iostream>

#include <configfile.hpp>
#include <paths.hpp>
#include <error.hpp>
#include <tomlcpp.hpp>

bool configFileOpen(struct optData* data) {
	auto configFile = toml::parseFile(prefixConfig + "config.toml");
	if (!configFile.table) {
		return false;
	}

	auto useHaarResult = configFile.table->getBool("use_haar");
	if (useHaarResult.first) {
		data->useHaar = useHaarResult.second;
	}
	auto showCameraResult = configFile.table->getBool("show_camera");
	if (showCameraResult.first) {
		data->showCamera = showCameraResult.second;
	}
	auto noEyesResult = configFile.table->getBool("no_eyes");
	if (noEyesResult.first) {
		data->noEyes = noEyesResult.second;
	}
	auto modelNameResult = configFile.table->getString("model");
	if (modelNameResult.first) {
		data->model = modelNameResult.second;
	}
	auto cameraResult = configFile.table->getInt("min_camera_id");
	if (cameraResult.first) {
		data->minCameraID = cameraResult.second;
	}

	return true;
}
