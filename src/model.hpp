#ifndef MODEL_HPP
#define MODEL_HPP

#include <vector>
#include <modelpart.hpp>
#include <cv.hpp>

class Model {
	std::vector<ModelPart> parts;

	std::string name;

	std::string artist;
	std::string modeler;
	std::string license;

	public:
		Model(const char* path);

		void draw();

		void updateTransforms(struct FaceData faceData);

		std::string getName();
		std::string getInfoString();
};

#endif
