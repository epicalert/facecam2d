#include <graphics.hpp>
#include <cv.hpp>
#include <paths.hpp>
#include <args.hpp>
#include <config.hpp>
#include <model.hpp>
#include <configfile.hpp>

#include <iostream>
#include <cstring>

int main (int argc, char** argv) {
	std::cout << PROJECT_NAME " is starting..." << std::endl;

	initPrefixes();
	makePaths();

	// load config file and apply it
	configFileOpen(&optData);

#ifndef _WIN32
	// parse arguments and apply them
	argp_parse(&argp, argc, argv, 0, 0, 0);
#endif


	std::cout << "Custom asset prefix: " << prefixCustom << std::endl;
	std::cout << "Default asset prefix: " << prefixDefault << std::endl;

	initGraphics();

	//Model model(resolvePath("models/test.fma").c_str());
	initCV();


	while (true) {
		cvFrame();

		graphicsFrame();

#ifndef __APPLE__
		cvShowFrame();
#endif
	}
}
