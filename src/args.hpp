#ifndef ARGS_HPP
#define ARGS_HPP

#include <string>

#ifndef _WIN32
#include <argp.h>

error_t parseOptions(int key, char* arg, struct argp_state* state);
#endif

struct optData {
	bool useHaar;		// use haar cascades (0x00)
	bool showCamera;	// show camera feed (0x01)
	bool noEyes;		// disable eye tracking (0x02)
	std::string model;	// model to open (0x6d 'm')
	int minCameraID;	// camera id to start search on (0x63 'c')
};

extern const char* argp_program_version;
extern const struct argp_option options[];
extern struct argp argp;
extern struct optData optData;

#endif
