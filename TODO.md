# Before 1.0

## docs
- getting started guide
- demo video?

## models
- clipping (alpha stencil)

## Bugs
- incorrect appdata location on Windows

# Whenever

- log file
- i18n
- remove configured files with make clean

## vision
- eye open/closed

## graphics
- in-window text rendering

