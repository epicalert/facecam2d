.TH FC2D_MODEL 5 "Facecam2D Model Format 0.5 Manual"
.
.SH NAME
fc2d_model \- Facecam2D model file format
.
.
.SH DESCRIPTION
Facecam2D uses
.B zip
archive files using the
.I .fma (Facecam2D model archive)
file extension, to store models.
.
These zip files contain a mandatory
.I toml
file called
.B model.toml,
which describes the model and its parts.
.
This file is called the
.B model descriptor.
.
Any model file that does not contain
.B model.toml
in the root of the archive is invalid.
.
.PP
Example
.B model.toml:
.
.sp
.
.EX
[format]
version_major = 0
version_minor = 3

[model_info]
name = "Example Model"
version = "1.0"

[[part]]
texture = "tex/head.png"
bind = "head"

[[part]]
bind = "head"
follow = "face"
factor = 0.75

	[[textures]]
	file = "tex/face-closedmouth.png"

	[[textures]]
	file = "tex/face-openmouth.png"
	trigger = "mouth-open"
.EE
.
.PP
.
In addition to the model descriptor, the model archive can contain other
files (e.g. textures) that can be anywhere in the archive, including the root.
.
Unnecessary files should not be present in the archive, but archives with
unnecessary files are still valid.
.
.
.SH FORMAT VERSION
.PP
.
Every Facecam2D model has a version code in the 
.B model.toml
in the format "major.minor".
A given version of Facecam2D can read model files with the same
major version as the version it is compatible with.
For example, if Facecam2D can read the model format up to version 0.3,
the same build of Facecam2D can also read a model written in format 0.2,
but cannot play a model written in format 1.2 or 0.5.
It is only backwards compatible with the same major version.
Higher minor versions of the model format indicate that there are newer
optional features available, but does not break compatibility with formats
of the same major version.
.
.
.SH KEYS
.SS format
.TP
version_major
Integer: Major version of the model format (e.g. 0 for v0.3).
.
.TP
version_minor
Integer: Minor version of the model format (e.g. 3 for v0.3).
.
.
.SS model_info
.TP
name
String: The name of the model (will be displayed in the window title).
.
.TP
version
String: A version string for the model (currently unused).
.
.TP
artist
.ft B
(Since version 0.5)
.ft
String: Name of artist or photographer
.
.TP
modeler
.ft B
(Since version 0.5)
.ft
String: Name of modeler or rigger
.
.TP
license
.ft B
(Since version 0.5)
.ft
String: SPDX license identifier (if none model is assumed to be proprietary)
.
.
.SS part
.TP
texture
String: Path to a texture file in the archive. Required if
.B [[textures]]
is not present in the part.
.
.TP
bind
String: A bind point which the part will be bound to.
Possible values: null, head, face
.
.TP
follow
String: A bind point which the part will follow depending on
.B factor.
The part will copy the position of
.B bind
but move in the direction of
.B follow.
.
.TP
factor
Float: How far the part will move towards the
.B follow
point.
If negative, the part will move in the opposite direction.
.
.TP
rot_factor, scale_factor
.ft B
(Since version 0.2)
.ft
Float: How much the part will rotate/scale.
Default is 1.0.
Higher values result in more exaggerated rotation or scaling.
.
.TP
offset_factor
.ft B
(Since version 0.4)
.ft
float: How much the part will be moved by the offset bind.
See
.B offset_bind.
.
.TP
origin
.ft B
(Since version 0.3)
.ft
Float Array: Origin (pivot point) of the model defined in
normalized coordinates.
Default is [0.0, 0.0].
.
.TP
pos_offset, scale_offset
.ft B
(Since version 0.3)
.ft
Float Array: Positional offset or scale of a part.
Default is [0.0, 0.0] for
.B pos_offset
and [1.0, 1.0] for
.B scale_offset.
.
.TP
offset_bind
.ft B
(Since version 0.4)
.ft
String: Offsets are vectors instead of actual points on the image.
You can bind to an offset with this property, which will
offset the part's position according to the vector.
Possible values: offset-eyes
.
.SS textures
.TP
file
Path to a texture file in the archive. Required if
.B texture
is not present in the part.
.
.TP
trigger
A trigger that will cause the part to use this texture.
Possible values: null, mouth-open
.
.PP
When specifying multiple textures, the first one specified
will be used by default.
