find_path( GLUT_INCLUDE_DIR NAMES GL/freeglut.h )

find_library( GLUT_LIBRARY NAMES glut freeglut PATHS /usr/lib /usr/lib64 /lib /lib64 /usr/local/lib )

if (GLUT_INCLUDE_DIR AND GLUT_LIBRARY)
	set( GLUT_FOUND "TRUE" )
	message( STATUS "Found FreeGLUT: ${GLUT_LIBRARY}" )
endif (GLUT_INCLUDE_DIR AND GLUT_LIBRARY)
