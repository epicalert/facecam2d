find_path( WEBP_INCLUDE_DIR NAMES webp/decode.h )

find_library( WEBP_LIBRARY NAMES webp PATHS /usr/lib /usr/lib64 /lib /lib64 /usr/local/lib )

if (WEBP_INCLUDE_DIR AND WEBP_LIBRARY)
	set(WEBP_INCLUDE_DIR ${WEBP_INCLUDE_DIR})
	set(WEBP_LIBRARIES ${WEBP_LIBRARY})
	set( WEBP_FOUND "TRUE" )
endif (WEBP_INCLUDE_DIR AND WEBP_LIBRARY)
