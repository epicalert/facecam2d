find_path( LIBZIP_INCLUDE_DIR NAMES zip.h )

find_library( LIBZIP_LIBRARY NAMES zip PATHS /usr/lib /usr/lib64 /lib /lib64 /usr/local/lib )

if (LIBZIP_INCLUDE_DIR AND LIBZIP_LIBRARY)
	set( LIBZIP_FOUND "TRUE" )
	message( STATUS "Found libzip: ${LIBZIP_LIBRARY}" )
endif (LIBZIP_INCLUDE_DIR AND LIBZIP_LIBRARY)
