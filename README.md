# Facecam2D
2D avatar facecam software

---

## Installation



### Debian GNU/Linux

A `.deb` package is available for systems running **Debian 11 or newer** or **Ubuntu 20.04 or newer.** You can find it on the Releases page.

### Arch Linux

An AUR package (`facecam2d`) is available for Arch Linux users.

### Windows

A Windows Installer package (MSI) is available for systems running **Windows 10 or newer.** You can find it on the Releases page. (You might be able to run it on earlier Windows versions, this hasn't been tested.) 

### Verifying packages (optional)

Released binaries are signed with this PGP key:
```
pub   rsa3072 2021-07-12 [SC] [expires: 2022-07-12]
      1CB1 F64B 8061 EB02 B8A4  B421 4AB7 1F23 5C64 BE77
uid           [ultimate] Rubi Wilkins (Facecam2D Release Signing Key) <epicalert@protonmail.com>
```
You can download it at https://epicalert.xyz/keys/4AB71F235C64BE77.asc

When downloading the package, download its accompanying signature and verify it using e.g. GnuPG.
```
gpg --verify facecam2d_0.3.0-1_amd64.deb.asc
```


## Building

### GNU/Linux or similar systems

Make sure you have all dependencies installed.

1. Create a `build` directory and cd into it.

```
mkdir build
cd build
```

2. Configure with `cmake` (You may need to define `wxWidgets_CONFIG_EXECUTABLE` and `wxWidgets_wxrc_EXECUTABLE` if you're using Arch Linux.)

```
cmake ..
```
3. Compile with `make`

```
make -j4
```

4. Run the Facecam2D Configuration Tool and/or Facecam2D 

```
./fc2dconfig
./fc2d
```

5. Optionally, you can also install your build.

```
make install
```

### Windows

At the moment, building on Windows is not supported. You will need to cross-compile from a GNU/Linux system with MinGW.

1. Create a `build` directory and cd into it.

```
mkdir build
cd build
```

2. Configure with the MinGW version of `cmake`

```
x86_64-w64-mingw32-cmake ..
```
3. Compile with `make`

```
make -j4
```
4. Make SourceDir for MSI packaging

```
mkdir SourceDir
cp -r *.exe libs/Boxer/libBoxer.dll ../packaging/*.ico models/ cvdata/ SourceDir/
```

6. Use a utility like peldd to find and copy the DLLs needed

```
for lib in $(peldd -t -r -w OPENGL32.dll -w libBoxer.dll fc2d.exe)
	cp $lib SourceDir
for lib in $(peldd -t -r fc2dconfig.exe)
	cp $lib SourceDir
```

7. Create MSI package with `wixl`

```
wixl ../packaging/*.wx(s|i) -o facecam2d-$(git describe).msi
```
